import requests

class Project:
    def __init__(self, name, commits_url, language):
        self.name = name
        self.commits_url = commits_url
        self.number_of_commits = 0;
        if language == None:
            self.language = 'Not specified'
        else:
            self.language = language
    
     def count_commits(self, username, token):
        params = {'page': 1, 'per_page': 30}
        api = self.commits_url
        r = requests.get(api, params= params, auth=(username, token))
        json = r.json();
        while(len(json)==30):
            self.number_of_commits = self.number_of_commits + 30
            params['page'] = params['page'] + 1
            r = requests.get(api, params= params, auth=(username, token))
            json = r.json();
        for commit in json:
            self.number_of_commits = self.number_of_commits + 1
    
    def print_project(self):
        print('Name: '+self.name+' Number of Commits: '+str(self.number_of_commits)+' Main Language : ' +self.language)
            
username = input("Enter the github username:")
token = input("Enter the access token:")
response = requests.get('https://api.github.com/user/repos',auth=(username, token) )
json = response.json()
projects = []
for i in range(0,len(json)):
    name = json[i]['name']
    print('Processing ' + name)
    commits_url = json[i]['commits_url'].split('{')[0]
    language = json[i]['language']
    repo = Project(name, commits_url, language)
    repo.count_commits(username, token)
    projects.append(repo)

for repo in projects:
    repo.print_project();

exit()