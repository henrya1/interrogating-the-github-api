# Interrogating the Github API #


### What is this repository for? ###

* This python script accesses the github api for the logged in user, through a user provided username and access token, and displays some useful data

### Prerequisites ###
* Python 3.8.5
* Having a personal access token for authentication from Github - Please see link for information on how to get an personal access token https://docs.github.com/en/free-pro-team@latest/github/authenticating-to-github/creating-a-personal-access-token

### How do I get set up? ###

* To get started, clone the repositery to your own machine and navigate to the code \github-access\.idea in the comand line
* To run the script type python github.py
* You will be prompted to enter the username and your access token
* The script will then process all the repos from the logged in Github account and display some data about them in the comand line



